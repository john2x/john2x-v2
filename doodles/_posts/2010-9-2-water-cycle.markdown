---
title: Water Cycle
layout: post
category: doodles
description: Stylized illustration depicting the water cycle.
---

![water-cycle]({{site.repo}}images{{page.url}}/watercycle-colored.jpg "Water Cycle")

