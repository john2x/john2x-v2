---
title: 3D Sculptures
layout: post
category: doodles
description: Some old 3D models.
---
![male head]({{site.repo}}images{{page.url}}/male-head.jpg "Male Head")

![creature]({{site.repo}}images{{page.url}}/creature.jpg "Creature")

![female-head]({{site.repo}}images{{page.url}}/female-head.jpg "Female Head")

![cartoon-girl]({{site.repo}}images{{page.url}}/cartoon-girl.jpg "Cartoon Girl")

![dino]({{site.repo}}images{{page.url}}/dino.jpg "Dino")

![fat-creature]({{site.repo}}images{{page.url}}/fat-creature.jpg "Fat Creature")

![old-alien]({{site.repo}}images{{page.url}}/old-alien.jpg "Old Alien")

![big-alien]({{site.repo}}images{{page.url}}/big-alien.jpg "Big Alien")

