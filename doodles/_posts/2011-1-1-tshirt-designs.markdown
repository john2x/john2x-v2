---
title: "T-Shirt Designs"
layout: post
category: doodles
description: Various t-shirt designs I've done over time. 
---
<h2 id="i-heart-you">I Heart You</h2>

![Heart]({{site.repo}}images{{page.url}}/heart.jpg)

<h2 id="kini">Kini </h2>

### Black
![Kini Black]({{site.repo}}images{{page.url}}/kini-shirt.jpg)
### White
![Kini White]({{site.repo}}images{{page.url}}/kini-shirt2.jpg)

<h2 id="uc-t-shirt">UC T-Shirt</h2>

### Front
![UC]({{site.repo}}images{{page.url}}/IT-front.jpg)
### Back
![UC]({{site.repo}}images{{page.url}}/IT-back.jpg)

<h2 id="tweet-shirt">Tweet Shirt</h2>

![Tweet Shirt]({{site.repo}}images{{page.url}}/twittershirt.jpg)

<h2 id="wheatgrass">Wheatgrass</h2>

![Wheatgrass]({{site.repo}}images{{page.url}}/wheatgrass.jpg)

