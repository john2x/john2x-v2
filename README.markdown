#Hello!

This is the source for v2 of my website.

It uses:

- [HTML5 Boilerplate][h5bp]
- [Jekyll][] on [Github Pages][pages]
- [960gs]

Thank you for visiting!

[h5bp]: http://h5bp.com
[Jekyll]: http://jekyllrb.com
[pages]: http://pages.github.com
[960gs]: http://960.gs

