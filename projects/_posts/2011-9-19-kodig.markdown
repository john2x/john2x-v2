---
title: Kodig
layout: post
category: projects
description: Quick posts for everyone.
---
What is Kodig?
----
[Kodig][kodig] is a free service for sharing quick posts online. You can post anything from detailed articles to random rants. If you are a coder, you can consider it as a [pastebin][] for non-code related texts.  

[pastebin]: http://pastebin.com
[kodig]: http://kodig.com

Kodig is open source
-----
I've decided to open [source][] Kodig in the hopes of learning more out of it from others and also maybe others can learn from it as well.

It uses a 3-clause BSD license. Check out the LICENSE file in the [source][] directory.

[source]: https://bitbucket.org/john2x/kodig

What's with the name?
-----
[Kodig][kodig] is slang in my dialect for little notes or "cheat sheets". 

Where can I send feedback/bug reports/complaints?
-----
You can email john2x at gmail.com

Screenshots
-----

![Kodig]({{site.repo}}images{{page.url}}/kodig.png)

![New Post]({{site.repo}}images{{page.url}}/kodig-new.png)

![Post]({{site.repo}}images{{page.url}}/kodig-post.png)

