---
title: Sunrise
layout: post
category: doodles
description: A photo blogging WordPress theme. 
---
<h2 id="what-is-sunrise">What is Sunrise?</h2>
Sunrise is a free WordPress photo blogging theme. It is inspired by magazine foldouts and poster design. 

### Features ###

- Full background photos
- Simple and minimalist design
- Use WordPress' "Featured Image" for your photos
- "Show/Hide Info" toggle for a better view of your photos
- Display thumbnails on homepage for better viewing 

### Limitations ###

- Categories don't work (yet)
- Opera doesn't play too well with the full background image
- Internet Explorer doesn't support transparent backgrounds, so the text will fallback to a non-transparent background
- Comments are disabled (no plans on adding them yet)


View the [demo][] to see how it looks.

[Download][] and start using it on your photo blog.

The source is on [Bitbucket][] and [Github][]. Feel free to modify it. Some attribution would be nice though. 

Read the [blog post][blogpost] for a more detailed writeup. 


<h2 id="screenshots">Screenshots</h2>
![Homepage]({{site.repo}}images{{page.url}}/index.png)

![Post with info]({{site.repo}}images{{page.url}}/single-with-info.png)

![Post with info hidden]({{site.repo}}images{{page.url}}/single-no-info.png)

![Sample page]({{site.repo}}images{{page.url}}/page.png)

[blogpost]: {{site.url}}/blog/sunrise-a-wordpress-theme
[Bitbucket]: {{site.bitbucket}}/sunrise
[Github]: {{site.github}}/sunrise
[Download]: {{site.bitbucket}}/sunrise/get/v1.1.0.zip
[demo]: {{site.url}}/wordpress

