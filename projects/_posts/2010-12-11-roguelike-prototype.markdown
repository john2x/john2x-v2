---
title: Roguelike Prototype
layout: post
category: projects
description: An early prototype for a roguelike game.
---
## Biting Off More Than I Can Chew
I first heard about [roguelikes](http://en.wikipedia.org/wiki/Roguelike) on [reddit](http://reddit.com/r/roguelikes) and I immediately had an idea for one (it involved zombies). I thought it wouldn't be too hard, but I overestimated my abilities. 

I plan on getting back on this in the future. I've learned a lot since then and I hope to make it through next time. 

## Early Video
The framerate is too high in this. 
I got map generation from a text file, movement and basic collision detection with walls and other objects. I think I got a door working as well. 

<object width="582" height="290"><param name="movie" value="http://www.youtube.com/v/1ll-dxuuIPE?fs=1&amp;hl=en_US&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/1ll-dxuuIPE?fs=1&amp;hl=en_US&amp;rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="582" height="290"></embed></object>

