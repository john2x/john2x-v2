
---
title: r.doqdoq
layout: post
category: projects
description: Browse Reddit like you're reading code.
---
## What is r.doqdoq.com?
[r.doqdoq](http://r.doqdoq.com) was supposed to be called coderedd.it, but I couldn't buy the domain so I had to make do with what I had. 

It is a website which allows you to browse [Reddit][] like you're reading code in your favorite programming language. 

It runs on [Flask][] using the [reddit\_api][api] library.

Read through the Reddit [thread][] for more info or check out the [source][] and contribute a template. 

[Flask]: http://flask.pocoo.org
[Reddit]: http://reddit.com
[api]: https://github.com/mellort/reddit_api
[thread]: http://www.reddit.com/r/Python/comments/k6enr/a_website_that_lets_you_browse_reddit_like_youre/
[source]: {{site.bitbucket}}/rdoqdoq

Screenshots
-----

![Python]({{site.repo}}images{{page.url}}/rdoqdoq-python.png)

![Java]({{site.repo}}images{{page.url}}/rdoqdoq-java.png)

![Comments]({{site.repo}}images{{page.url}}/rdoqdoq-comments.png)

