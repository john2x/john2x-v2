---
title: Color Mixer
layout: post
category: projects
description: Online color mixer. 
---
## If You Can't Find One, Build One
I've searched high and low for a free color mixer. I didn't need a lot of features. 

Basically I just wanted to be able to mix colors using RGB and HSV sliders to get to that perfect color. I also wanted to be able to pick any color on the screen. 

Eventually I gave up on searching and decided to just build my own. I was learning Flex then and I thought that it would be a good project to learn with. Unfortunately, it is impossible to get any color from the screen outside the stage with Flash. AIR, at that time, also didn't have any such features. 

## Screenshots
![John's Color Mixer]({{site.repo}}images{{page.url}}/colormixer.jpg "John's Color Mixer") 

## Try it Out
[John's Color Mixer](http://john2x.com/media/uploads/ColorManipulation/ColorMixer.html)

